<?php

/**
 * @file
 * Custom Drush integration.
 */

/**
 * Implements hook_drush_command().
 *
 * @return array
 *   An associative array describing your command(s).
 */
function devit_drush_command() {
  return array(
    'devit' => array(
      'callback' => 'devit_drush',
      'description' => dt('Puts your site in dev mode.'),
    ),
  );
}

/**
 * Put the site in Dev It.
 */
function devit_drush() {
  devit_devit();
  drush_log('Site ready for development!', 'success');
}

/**
 * Enables an array of modules and displays message to user.
 */
function devit_drush_enable_modules($modules) {
  module_enable($modules, TRUE);
  foreach ($modules as $module) {
    if (module_exists($module)) {
      drush_log(dt('@module enabled.', array('@module' => $module), 'success'));
    }
    else {
      drush_log(dt('@module could not be found and was not enabled!', array('@module' => $module), 'error'));
    }
  }
}
