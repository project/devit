<?php

/**
 * Implements hook_devit_tasks().
 *
 * Use this hook to define new tasks that should be available to devit.
 */
function devit_tasks_devit_tasks() {
  $tasks = array();

  $tasks['clear-caches'] =  array(
    'title' => t(''),
    'description' => t(''),
    'task callback' => '',
    'task arguments' => array(),
    'access arguments' => array(),
    'access callback' => '',
    'file' => '',
    'file path' => '',
    'include file' => '',
    'weight' => 0,
    'status' => 0,
    'drush only' => FALSE,
  );

  return $tasks;
}
