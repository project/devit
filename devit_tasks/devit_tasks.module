<?php

/**
 * Implements hook_devit_tasks().
 */
function devit_tasks_devit_tasks() {
  $tasks = array();

  $tasks['clear-caches'] = array(
    'title' => t('Clear caches'),
    'description' => t('Clears all caches.'),
    'task callback' => 'devit_tasks_clear_caches',
    'weight' => 1000,
    'access arguments' => array('administer site configuration'),
  );
  $tasks['disable-caching'] = array(
    'title' => t('Disable core caching features'),
    'description' => t('Disables page cache, page compression, block cache, and CSS & JS optimization.'),
    'task callback' => 'devit_tasks_disable_caching',
    'access arguments' => array('administer site configuration'),
  );
  $tasks['update-file-paths'] = array(
    'title' => t('Update Drupal file paths'),
    'description' => t('The user will be prompted to enter new file paths'),
    'task callback' => 'devit_tasks_update_file_paths',
    'drush only' => TRUE,
    'access arguments' => array('administer site configuration'),
  );
  if (module_exists('reroute_mail')) {
    $tasks['reroute-mail'] = array(
      'title' => t('Enable mail rerouting'),
      'task callback' => 'devit_tasks_reroute_mail',
    );
  }
  $tasks['update-error-level'] = array(
    'title' => t('Update error level'),
    'description' => t('Sets error level to 2'),
    'task callback' => 'devit_tasks_update_error_level',
    'access arguments' => array('administer site configuration'),
  );
  $tasks['update-user-passwords'] = array(
    'title' => t('Overwrite all user passwords'),
    'description' => t('Overwrites all user passwords with user-specified value.'),
    'task callback' => 'devit_tasks_update_user_passwords',
    'access arguments' => array('administer users'),
  );

  return $tasks;
}

/**
 * Clears caches.
 */
function devit_tasks_clear_caches() {
  drupal_flush_all_caches();
  drupal_set_message(t('All caches cleared.'));
}

/**
 * Disables core caching.
 */
function devit_tasks_disable_caching() {
  variable_set('cache', "0");
  variable_set('block_cache', "0");
  variable_set('preprocess_js', "0");
  variable_set('preprocess_css', "0");
  variable_set('page_compression', "0");
  drupal_set_message(t('Page cache, page compression, JS optimization, and CSS optimization disabled.'));
}

/**
 * Sets error level.
 */
function devit_tasks_update_error_level($error_level = 2) {
  variable_set('error_level', $error_level);
}

/**
 * Updates Drupal file paths.
 */
function devit_tasks_update_file_paths() {
  $variables = array(
    // Prompt the user to override files directory.
    'file_directory_path' => array(
      'default value' => 'sites/files',
      'prompt' => "Set files directory path",
      'accepted message' => 'Files directory path changed to @new_value.',
      'overridden message' => 'Files directory path changed to @new_value.',
    ),
    // Prompt the user to override temp files directory.
    'file_directory_temp' => array(
      'default value' => 'sites/files/tmp',
      'prompt' => "Set temporary files directory path",
      'accepted message' => 'Temporary files directory path changed to @new_value.',
      'overridden message' => 'Temporary files directory path changed to @new_value.',
    ),
  );
  devit_drush_variable_set_prompt($variables);
}

/**
 * Enables email rerouting.
 */
function devit_tasks_reroute_mail($email = '') {
  if (module_exists('reroute_email')) {
    variable_set('reroute_email_enable', 1);
    drupal_set_message(t('Email rerouting enabled.'), 'success');
    $variables = array(
      'reroute_email_address' => array(
        'default value' => !empty($email) ? $email : variable_get('site_mail'),
        'prompt' => 'Enter destination email address for rerouted emails',
        'accepted message' => 'Email is being rerouted to @new_value.',
        'overridden message' => 'Email is being rerouted to @new_value.',
      ),
    );
    devit_drush_variable_set_prompt($variables);
  }
}

/**
 * Enables and configures secure site.
 */
function devit_secure_sites() {
  // Secure site settings.
  if (module_exists('securesite')) {
    variable_set('securesite_enabled', 1);
    variable_set('securesite_type', array(1));
    drupal_set_message(t('Secure site protection enabled.'));

    // Enable guest account access.
    $guest_name = 'anonuser';
    $guest_pass = 'anonuser';
    variable_set('securesite_guest_name', $guest_name);
    variable_set('securesite_guest_pass', $guest_pass);
    devit_add_permissions(DRUPAL_ANONYMOUS_RID, array('access secured pages'));
    drupal_set_message(t('Guest access enabled.'));
    drupal_set_message(t('Guest username: anonuser', array('@guest_name' => $guest_name)));
    drupal_set_message(t('Guest password: anonuser', array('@guest_pass' => $guest_pass)));
  }
}

/**
 * Overwrites all user passwords.
 */
function devit_tasks_update_user_passwords($password = 'password') {
  if (devit_verify_cli()){
    $prompt = dt('Please enter the new password for all users');
    $password  = drush_prompt($prompt, $password);
  }

  // Override all user passwords.
  require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
  $password = user_hash_password($password);
  db_query("UPDATE {users} SET pass = :password", array(':password' => $password));
  drupal_set_message(t("All users passwords have been changed to @password.", array('@password' => $password)));
}
