<?php
/**
 * Form callback for Dev It admin form.
 */
function devit_ui_admin_form() {
  $form = array();

  $tasks = devit_build_tasks();
  $fieldsets = array();
  foreach ($tasks as $task_name => $task) {
    $task_module = $task['module'];

    // Build fieldset for this module.
    if (empty($form[$task_module])) {
      $form[$task_module] = array(
        '#type' => 'fieldset',
        '#title' => t('@module_name', array('@module_name' => $task_module))
      );
    }

    // Add form checkbox for this task.
    $form[$task_module]['devit_task_' . $task_name] = array(
      '#type' => 'checkbox',
      '#title' => !empty($task['title']) ? $task['title'] : t('@task_name', array('@task_name' => $task_name)),
      '#description' => !empty($task['description']) ? $task['description'] : '',
      '#default_value' => variable_get('devit_task_' . $task_name, $task['status']),
    );

    if ($task['drush only']) {
      $form[$task_module]['devit_task_' . $task_name]['#description'] .= t('<br /> *May only be executed via Drush.');
    }
  }

  $form['devit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and devit!'),
    '#submit' => array('devit_ui_confirm_devit'),
  );

  return system_settings_form($form);
}

/**
 * Confirms devit submission.
 */
function devit_ui_confirm_devit($form, &$form_state) {
  system_settings_form_submit($form, &$form_state);
  drupal_goto('admin/config/development/devit/execute');
}

/**
 * Displays form to confirm devification.
 */
function devit_ui_devit_form() {
  $form = array();
  $question = t('Are you sure that you want to devit this site?');
  $path = 'admin/config/development/devit';
  $description = t('Warning: this process may be IRREVERSIBLE! Depending on the tasks that you have selected, you may not be able to undo this!');

  return confirm_form($form, $question, $path, $description);
}

/**
 * Devifies site.
 */
function devit_ui_devit_form_submit($form, &$form_state) {
  devit_devit();
  drupal_goto('admin/config/development/devit');
}
