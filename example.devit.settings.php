<?php

/**
 * @file
 * Settings file for devit tasks.
 *
 * This file must renamed devit.settings.php and placed in your site directory!
 * E.g, place this at /sites/default/devit.settings.php. Settings specified in
 * this file will override any conflicting setting stored in the database.
 */

// Set task status with a simple, flat array. Tasks set to TRUE will be
// executed when devit is run.
$tasks = array(
  // Key rows by task name.
  'clear-caches' => TRUE,
  'update-file-paths' => FALSE,
);
